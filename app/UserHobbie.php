<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHobbie extends Model
{
    protected $table = "user_hobbies";
    protected $fillable = ['user_id','hobbies'];
    
}
