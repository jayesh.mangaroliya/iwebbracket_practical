<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\User;
use App\UserHobbie;
class RegisteruserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with('hobby')->paginate(5);
        
        return view('index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'image' => ['required'],
            'dob' => ['required'],
            'gender' => ['required'],
            'city' => ['required'],
            'hobbies' => ['required'],
        ]);

        $user = $request->all();
        //dd($user);
        $image = $request->file('image');
        $destinationPath = public_path('/images');
        $new_name = rand().'.'.$image->getClientOriginalExtension(); 
        $image->move($destinationPath, $new_name);
        
        $user['image'] = $new_name;
        $usersave = User::create($user);

        foreach($request->get('hobbies') as $hobby)
        {
            UserHobbie::create([
                'user_id' => $usersave->id,
                'hobbies' => $hobby
            ]);
        }

        return redirect()->route('registeruser.index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'dob' => ['required'],
            'gender' => ['required'],
            'city' => ['required'],
        ]);

        $user = $request->all();
            //dd($user);
        
        
        $userupdate = User::find($id);
        $userupdate->name = $request->input('name');
        $userupdate->email = $request->input('email');
        $userupdate->dob = $request->input('dob');
        $userupdate->gender = $request->input('gender');
        $userupdate->city = $request->input('city');
        
        if ($request->file('image')!=null)
        {
            $image = $request->file('image');
        $destinationPath = public_path('/images');
        $new_name = rand().'.'.$image->getClientOriginalExtension(); 
        $image->move($destinationPath, $new_name);
            $userupdate->image = $new_name;
        }
        else
        {
            $userupdate->image = $request->input('image-copy');
        }
        $userupdate->save();
       
        return redirect()->route('registeruser.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('registeruser.index');
    }
}
