<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('registeruser');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/registeruser','RegisteruserController');
Route::get('/delete/{id}', 'RegisteruserController@destroy')->name('delete');
