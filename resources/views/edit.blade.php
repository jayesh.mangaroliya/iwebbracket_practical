@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registeruser.update',$user->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row">
                            <label for="profile_pic" class="col-md-4 col-form-label text-md-right">{{ __('Profile Pic') }}</label>

                            <div class="col-md-6">
                                <input type="file" id="image" class="form-control @error('image') is-invalid @enderror" name="image">
                                <input type="hidden" id="image-copy" class="form-control " name="image-copy" value="{{$user->image}}">
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of birth') }}</label>

                            <div class="col-md-6">
                                <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ $user->dob }}" required autocomplete="name" autofocus>

                                @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6 form-control">
                                <input type="radio" id="male" name="gender" value="male" {{ ($user->gender=="male")? "checked" : "" }}>
                                <label for="html">Male</label>
                                <input type="radio" id="female" name="gender" value="female" {{ ($user->gender=="female")? "checked" : "" }}>
                                <label for="css">Female</label><br>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                            <div class="col-md-6">
                            <select name="city" id="city" class="form-control">
                                <option value="amd" {{ $user->city == "amd" ? 'selected' : '' }}>Ahmedabad</option>
                                <option value="raj" {{ $user->city == "raj" ? 'selected' : '' }}>Rajkot</option>
                                <option value="amr" {{ $user->city == "amr" ? 'selected' : '' }}>Amreli</option>
                                <option value="srt" {{ $user->city == "srt" ? 'selected' : '' }}>Surat</option>
                            </select>

                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <?php   $hb = App\UserHobbie::where('user_id','=',$user->id)->select('hobbies')->get(); ?>

                        <div class="form-group row">
                            <label for="hobbies" class="col-md-4 col-form-label text-md-right">{{ __('Hobbies') }}</label>

                            <div class="col-md-6 form-control">
                            <input type="checkbox" name="hobbies[]" value="chess" {{  ($hb == "chess" &&  $hb == "cricket" && $hb == "singin" ? ' checked' : '')}} > Chess &nbsp;&nbsp;
                            <input type="checkbox" name="hobbies[]" value="cricket" {{  ($hb == "chess" &&  $hb == "cricket" && $hb == "singing" ? ' checked' : '')}}> Cricket &nbsp;&nbsp;
                            <input type="checkbox" name="hobbies[]" value="singing" {{  ($hb == "chess" &&  $hb == "cricket" && $hb == "singing" ? ' checked' : '')}}> Singing

                                @error('hobbies')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
