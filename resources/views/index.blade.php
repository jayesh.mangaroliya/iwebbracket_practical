<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Register User</h2>
<a class="button primary add" style="text-align: right;" href="{{route('registeruser.create')}}">Add</a>
<table>
  <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Image</th>
    <th>Gender</th>
    <th>City</th>
    <th>Hobby</th>
    <th>Action</th>
  </tr>
  <tr>
    @foreach($user as $users)
    <td>{{ $users->name }}</td>
    <td>{{ $users->email }}</td>
    <td><img src="{{ asset('images') . '/' . $users->image }}" alt="" class="image" style="width: 80px;height: 80px">
        </td>
    <td>{{ $users->gender }}</td>
    <td>{{ $users->city }}</td>
    <?php   $hb = App\UserHobbie::where('user_id','=',$users->id)->pluck('hobbies'); ?>
    <td>{{ $hb }}</td>
    <td><a class="button primary edit" href="{{route('registeruser.edit',$users->id)}}">Edit</a>
    <a class="button primary delete" href="{{route('delete',$users->id)}}">Delete</a></td>
    @endforeach
  </tr>
 
</table>
{{ $user->links() }}
</body>
</html>